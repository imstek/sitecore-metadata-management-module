﻿define(["sitecore", "/-/speak/v1/ExperienceEditor/ExperienceEditor.js"], function (Sitecore, ExperienceEditor) {
    Sitecore.Commands.LaunchFieldEditor =
    {
        canExecute: function (context) {
            var isEnabled = false;

            context.currentContext.argument = context.button.viewModel.$el[0].accessKey;
            ExperienceEditor.PipelinesUtil.generateRequestProcessor(
                "ExperienceEditor.FieldExist",
                function (response) {
                    isEnabled = response.responseValue.value;
                }
            ).execute(context);
            return isEnabled;
        },
        execute: function (context) {
            context.currentContext.argument = context.button.viewModel.$el[0].accessKey;
            ExperienceEditor.PipelinesUtil.generateRequestProcessor(
                "ExperienceEditor.GenerateFieldEditorUrl",
                function (response) {
                    var DialogUrl = response.responseValue.value;
                    var dialogFeatures = "dialogHeight: 500px;dialogWidth: 650px;";
                    ExperienceEditor.Dialogs.showModalDialog(DialogUrl, '', dialogFeatures, null);
                }).execute(context);

        }
    };
});