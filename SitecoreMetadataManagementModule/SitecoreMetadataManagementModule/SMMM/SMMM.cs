﻿using System;
using System.Collections.Generic;
using Sitecore.ExperienceEditor.Speak.Server.Contexts;
using Sitecore.ExperienceEditor.Speak.Server.Requests;
using Sitecore.ExperienceEditor.Speak.Server.Responses;
using Sitecore.Data;
using Sitecore.Text;
using Sitecore.Shell.Applications.ContentEditor;
using Sitecore.Diagnostics;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Linq;
using Sitecore.Collections;
using Sitecore.Data.Templates;

namespace SMMM
{
    public class SMMM
    {

        public static List<TemplateFieldItem> GetTemplateFields(TemplateItem template)
        {
            if (template.OwnFields.Length > 0)
            {
                var OwnFields = template.OwnFields;

                return OwnFields.ToList();
            }
            return null;
        }



        public static List<TemplateFieldItem> GetFieldList(Sitecore.Data.Fields.MultilistField hiddenValueTemplates)
        {

            if (hiddenValueTemplates != null)
            {

                var fieldList = new List<FieldDescriptor>();

                var fieldTemplateList = new List<TemplateFieldItem>();


                Sitecore.Data.Items.Item[] items = hiddenValueTemplates.GetItems();

              
                if (items != null && items.Count() > 0)
                {
                    List <TemplateFieldItem> outParam =new List<TemplateFieldItem>();

                    foreach (var item in items)
                    {

                        TemplateItem template = Sitecore.Context.Database.GetTemplate(item.ID);

                        if (template != null)
                        {
                            var x = SMMM.GetTemplateFields(template);
                            outParam.AddRange(x);
                        }
                    }

                return outParam;
                }
            }
            return null;

        }

    }
}