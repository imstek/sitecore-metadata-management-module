﻿using System;
using System.Collections.Generic;
using Sitecore.ExperienceEditor.Speak.Server.Contexts;
using Sitecore.ExperienceEditor.Speak.Server.Requests;
using Sitecore.ExperienceEditor.Speak.Server.Responses;
using Sitecore.Data;
using Sitecore.Text;
using Sitecore.Shell.Applications.ContentEditor;
using Sitecore.Diagnostics;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Linq;
using Sitecore.Collections;
using Sitecore.Data.Templates;
using SMMM;

namespace SMMM
{

    public class GenerateFieldEditorUrl : PipelineProcessorRequest<ItemContext>
    {

        public   string CreateFieldDescriptors()
        {
             Sitecore.Data.Fields.CheckboxField preserveSections = null ;
             Sitecore.Data.Fields.MultilistField hiddenValueTemplates = null;


            string guid = RequestContext.Argument;
           

            Item configItem = Sitecore.Context.Database.GetItem(ID.Parse(guid));

            if (configItem != null) { 
               hiddenValueTemplates = configItem.Fields["Manage Hidden Fields"];
               preserveSections= configItem.Fields["Preserve Sections"];
            }


            //get list of available fileds form temapls; compare current context to available filed and show only filed available in template AND current context
            var fieldTemplateList = SMMM.GetFieldList(hiddenValueTemplates);

            var fieldList = new List<FieldDescriptor>();

            foreach (TemplateFieldItem x in fieldTemplateList)
            {
                if (RequestContext.Item.Fields[x.Name] != null){
                      fieldList.Add(new FieldDescriptor(RequestContext.Item, x.Name));
                  }       
           
                }

                var fieldeditorOption = new FieldEditorOptions(fieldList);
                //check configuration and enable section grouping
                if (preserveSections != null && preserveSections.Checked)
                {
                    fieldeditorOption.PreserveSections = true;
                }
                    //Save item when ok button is pressed
                fieldeditorOption.SaveItem = true;

                return fieldeditorOption.ToUrlString().ToString();

        }
        public override PipelineProcessorResponseValue ProcessRequest()
        {
            return new PipelineProcessorResponseValue
            {
                Value = CreateFieldDescriptors()
            };
        }
    }
}
