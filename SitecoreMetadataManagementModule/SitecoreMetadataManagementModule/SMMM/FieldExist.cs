﻿using System;
using System.Collections.Generic;
using Sitecore.ExperienceEditor.Speak.Server.Contexts;
using Sitecore.ExperienceEditor.Speak.Server.Requests;
using Sitecore.ExperienceEditor.Speak.Server.Responses;
using Sitecore.Data;
using Sitecore.Text;
using Sitecore.Shell.Applications.ContentEditor;
using Sitecore.Diagnostics;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Linq;
using Sitecore.Collections;

namespace SMMM
{
    public class FieldExist : PipelineProcessorRequest<ItemContext>
    {

        public bool fieldExist()
        {
            int count = 0;

            string guid = RequestContext.Argument;
            Item configItem = Sitecore.Context.Database.GetItem(ID.Parse(guid));
            
            Sitecore.Data.Fields.MultilistField hiddenValueTemplates = configItem.Fields["Manage Hidden Fields"];

            var fieldTemplateList = SMMM.GetFieldList(hiddenValueTemplates);


            foreach (TemplateFieldItem x in fieldTemplateList)
            {
                if (RequestContext.Item.Fields[x.Name] != null)
                {
                    count++;
                }
            }

            return (count > 0) ? true : false;

        }

        public override PipelineProcessorResponseValue ProcessRequest()
        {
            return new PipelineProcessorResponseValue()
            {
                Value = fieldExist()
            };
        }
    }
}
