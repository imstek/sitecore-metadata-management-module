﻿define(["sitecore", "/-/speak/v1/ExperienceEditor/ExperienceEditor.js"], function (Sitecore, ExperienceEditor) {
    Sitecore.Commands.LaunchFieldEditor =
    {
        canExecute: function (context) {
            var isEnabled = false;

          
            ExperienceEditor.PipelinesUtil.generateRequestProcessor(
                "ExperienceEditor.FieldExist",
                function (response) {
                    isEnabled = response.responseValue.value;
                }
            ).execute();
            return isEnabled;
        },
        execute: function () {
         
            ExperienceEditor.PipelinesUtil.generateRequestProcessor(
                "ExperienceEditor.GenerateFieldEditorUrl",
                function (response) {
                    var DialogUrl = response.responseValue.value;
                    var dialogFeatures = "dialogHeight: 500px;dialogWidth: 650px;";
                    ExperienceEditor.Dialogs.showModalDialog(DialogUrl, '', dialogFeatures, null);
                }).execute();

        }
    };
});